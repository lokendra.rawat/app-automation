#!/bin/sh

set -e

PROJECT_ROOT=$(git rev-parse --show-toplevel)

cd $PROJECT_ROOT/client/paris

# echo "Creating .env file"
# echo "BASE_URL=${BASE_URL}
# FRESH_CHAT_ID=${FRESH_CHAT_ID}
# GOOGLE_MAPS_API_KEY=${GOOGLE_MAPS_API_KEY}
# SEGMENT_ID=${SEGMENT_ID}
# FACEBOOK_APP_ID=${FACEBOOK_APP_ID}
# PUBLIC_URL=${PUBLIC_URL}
# NODE_ENV=${NODE_ENV}
# GOOGLE_CLIENT_ID=${GOOGLE_CLIENT_ID}" > .env

# Install the dependencies
echo "Installing dependencies..."
yarn install

# Build
echo "Install bundle..."
# CI=false yarn build
gem install bundle
bundle install
